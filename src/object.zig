const std = @import("std");
const vm = @import("vm.zig");

const Allocator = std.mem.Allocator;

pub const ObjType = enum {
    String,
};

pub const ObjValue = struct {
    String: []u8,
};

pub const Object = struct {
    otype: ObjType,
    value: ObjValue,
    next: ?*Object = null,
};

pub fn allocateObject(
    vmach: *vm.VM,
    otype: ObjType,
    value: ObjValue,
) !*Object {
    var obj = try vmach.allocator.create(Object);
    obj.otype = otype;
    obj.value = value;

    obj.next = vmach.objs;
    vmach.objs = obj;
    return obj;
}

fn createString(vmach: *vm.VM, data: []u8) !*Object {
    return allocateObject(vmach, ObjType.String, ObjValue{ .String = data });
}

pub fn copyString(vmach: *vm.VM, data: []const u8) !*Object {
    var str = try vmach.allocator.alloc(u8, data.len);
    std.mem.copy(u8, str, data);
    return try createString(vmach, str);
}

/// Assumes it can take ownership of the given data.
pub fn takeString(vmach: *vm.VM, data: []u8) !*Object {
    return try createString(vmach, data);
}

pub fn printObject(stdout: var, obj: Object) !void {
    switch (obj.otype) {
        .String => try stdout.print("{}", obj.value.String),
        else => unreachable,
    }
}
