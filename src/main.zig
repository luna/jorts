const std = @import("std");

const Allocator = std.mem.Allocator;

// const Scanner = @import("scanner.zig").Scanner;
const chunk = @import("chunk.zig");
const vm = @import("vm.zig");

const InterpretResult = vm.InterpretResult;

//const Compiler = @import("compiler.zig").Compiler;

pub var hadError = false;

fn run(allocator: *Allocator, data: []u8) !void {
    var stdout_file = try std.io.getStdOut();
    const stdout = &stdout_file.outStream().stream;

    var vmach = try vm.VM.init(allocator, stdout, true);
    defer vmach.deinit();
    try vmach.interpret(data);
}

fn runWithVM(vmach: *vm.VM, data: []u8) !void {
    var stdout_file = try std.io.getStdOut();
    const stdout = &stdout_file.outStream().stream;

    defer vmach.deinit();
    try vmach.interpret(data);
}

pub fn doError(line: usize, message: []const u8) !void {
    try errorReport(line, "", message);
}

pub fn errorReport(line: usize, where: []const u8, message: []const u8) !void {
    var stdout_file = try std.io.getStdOut();
    const stdout = &stdout_file.outStream().stream;

    try stdout.print("[line {}] Error {}: {}\n", line, where, message);
    hadError = true;
}

fn runFile(allocator: *Allocator, path: []const u8) !void {
    var lox_file = try std.fs.File.openRead(path);
    defer lox_file.close();

    const total_bytes = try lox_file.getEndPos();
    var slice = try allocator.alloc(u8, total_bytes);
    _ = try lox_file.read(slice);

    run(allocator, slice) catch |err| {
        switch (err) {
            InterpretResult.Ok => {},
            InterpretResult.CompileError => std.os.exit(65),
            InterpretResult.RuntimeError => std.os.exit(70),
            else => return err,
        }
    };
}

fn runPrompt(allocator: *Allocator) !void {
    var stdout_file = try std.io.getStdOut();
    const stdout = &stdout_file.outStream().stream;

    var vmach = try vm.VM.init(allocator, stdout, true);
    defer vmach.deinit();

    while (true) {
        try stdout.print(">");
        var buffer = try std.Buffer.init(allocator, ""[0..]);

        var line = std.io.readLine(&buffer) catch |err| {
            if (err == error.EndOfStream) return;

            return err;
        };

        runWithVM(&vmach, line) catch |err| {
            switch (err) {
                InterpretResult.Ok => {},
                InterpretResult.CompileError => blk: {
                    try stdout.print("compile error.\n");
                },
                InterpretResult.RuntimeError => blk: {
                    try stdout.print("runtime error.\n");
                },
                else => return err,
            }
        };

        vmach.resetStack();
    }
}

pub fn main() anyerror!void {
    var arena = std.heap.ArenaAllocator.init(std.heap.direct_allocator);
    defer arena.deinit();
    var allocator = &arena.allocator;

    var args_it = std.process.args();

    var jorts_arg0 = try (args_it.next(allocator) orelse {
        // if you ever reach this, tell me what is your os lmao
        unreachable;
    });

    var lox_path = try (args_it.next(allocator) orelse {
        try runPrompt(allocator);
        return;
    });

    try runFile(allocator, lox_path);
}

pub fn oldMain() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.direct_allocator);
    defer arena.deinit();
    var allocator = &arena.allocator;

    var stdout_file = try std.io.getStdOut();
    var stdout = &stdout_file.outStream().stream;

    // this crashes zig??? lol
    // var chk = try chunk.Chunk.init(allocator);
    //var opcode_byte: u8 = @enumToInt(chunk.OpCode.Return);
    //try chk.write(chunk.OpCode.Return);
}
