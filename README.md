# jorts

a compiler for the lox language from https://craftinginterpreters.com

this is a learning project. the implemtation is based heavily off the C part
of the book, but also the Java part for the scanner.

## notes

 - jorts' lox bytecode is not compatible with any implementation.

## how do?

```
zig build run
```

and play around with it
